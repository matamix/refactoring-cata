package com.codurance.training.tasks;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class Console {
	
    private final BufferedReader in = null;
    private final PrintWriter out = null;
    
    public BufferedReader getIn() {
		return in;
	}

	public PrintWriter getOut() {
		return out;
	}

}
