package com.codurance.training.tasks;

import com.codurance.training.tasks.Console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.codurance.training.tasks.EnumInSwitch.TypeOf;

public final class TaskList implements Runnable {

	private final Map<String, List<Task>> tasks = new LinkedHashMap<>();

	
	BufferedReader reader;
	PrintWriter writer;
    
    private long lastId = 0;

    public void run() {
        while (true) {
        	System.out.print("> ");
            String command;
            try {
                command = in.readLine();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if (command.equals("quit")) {
                break;
            }
            execute(command);
        }
    }

    
    TypeOf[] typeOf = TypeOf.values();
    
    private void execute(String commandLine) {
        String[] commandRest = commandLine.split(" ", 2);
        String command = commandRest[0];
        
        for (TypeOf type : typeOf) {
        switch (type) {
            case SHOW:
                show();
                break;
            case ADD:
                add(commandRest[1]);
                break;
            case CHECK:
                check(commandRest[1]);
                break;
            case UNCHECK:
                uncheck(commandRest[1]);
                break;
            case HELP:
                help();
                break;
            default:
                error(command);
                break;
        }
    }
    }

    private void show() {
        for (Map.Entry<String, List<Task>> project : tasks.entrySet()) {
        	System.out.println(project.getKey());
            for (Task task : project.getValue()) {
            	System.out.printf("    [%c] %d: %s%n", (task.isDone() ? 'x' : ' '), task.getId(), task.getDescription());
            }
            System.out.println();
        }
    }

    private void add(String commandLine) {
        String[] subcommandRest = commandLine.split(" ", 2);
        String subcommand = subcommandRest[0];
        if (subcommand.equals("project")) {
            addProject(subcommandRest[1]);
        } else if (subcommand.equals("task")) {
            String[] projectTask = subcommandRest[1].split(" ", 2);
            addTask(projectTask[0], projectTask[1]);
        }
    }

    private void addProject(String name) {
        tasks.put(name, new ArrayList<Task>());
    }

    private void addTask(String project, String description) {
        List<Task> projectTasks = tasks.get(project);
        if (projectTasks == null) {
            System.out.printf("Could not find a project with the name \"%s\".", project);
            System.out.println();
            return;
        }
        projectTasks.add(new Task(nextId(), description, false));
    }

    private void check(String idString) {
        setDone(idString, true);
    }

    private void uncheck(String idString) {
        setDone(idString, false);
    }

    private void setDone(String idString, boolean done) {
        int id = Integer.parseInt(idString);
        for (Map.Entry<String, List<Task>> project : tasks.entrySet()) {
            for (Task task : project.getValue()) {
                if (task.getId() == id) {
                    task.setDone(done);
                    return;
                }
            }
        }
        System.out.printf("Could not find a task with an ID of %d.", id);
        System.out.println();
    }

    private void help() {
    	System.out.println("Commands:");
    	System.out.println("  show");
    	System.out.println("  add project <project name>");
    	System.out.println("  add task <project name> <task description>");
    	System.out.println("  check <task ID>");
    	System.out.println("  uncheck <task ID>");
    	System.out.println();
    }

    private void error(String command) {
    	System.out.printf("I don't know what the command \"%s\" is.", command);
    	System.out.println();
    }

    private long nextId() {
        return ++lastId;
    }
}
